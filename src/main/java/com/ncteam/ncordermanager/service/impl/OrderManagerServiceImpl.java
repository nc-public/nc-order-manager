package com.ncteam.ncordermanager.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ncteam.ncordermanager.bean.Order;
import com.ncteam.ncordermanager.repository.OrderManagerRepository;
import com.ncteam.ncordermanager.service.OrderManagerService;
@Service
public class OrderManagerServiceImpl implements OrderManagerService{
	
	@Autowired
	private OrderManagerRepository orderRepo;

	@Override
	public Order placeOrder(Order order) {
		return orderRepo.save(order);
	}

	@Override
	public List<Order> getAllOrderDetails() {
		return orderRepo.findAll();
	}

}
