package com.ncteam.ncordermanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcOrderManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NcOrderManagerApplication.class, args);
	}

}
