package com.ncteam.ncordermanager.dto;

import java.util.List;

import com.ncteam.ncordermanager.bean.Order;

public class OrderResponse {
	
	private String status;
	private List<Order> listOfOrder;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<Order> getListOfOrder() {
		return listOfOrder;
	}
	public void setListOfOrder(List<Order> listOfOrder) {
		this.listOfOrder = listOfOrder;
	}
	public OrderResponse(String status, List<Order> listOfOrder) {
		super();
		this.status = status;
		this.listOfOrder = listOfOrder;
	}
	public OrderResponse() {
		
	}
	

}
