package com.ncteam.ncordermanager.service;

import java.util.List;

import com.ncteam.ncordermanager.bean.Order;


public interface OrderManagerService {

	public Order placeOrder(Order order);

	public List<Order> getAllOrderDetails();

}
