package com.ncteam.ncordermanager.controller;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ncteam.ncordermanager.bean.Order;
import com.ncteam.ncordermanager.dto.OrderResponse;
import com.ncteam.ncordermanager.service.OrderManagerService;

@RestController
public class OrderManagerController {

	@Autowired
	private OrderManagerService orderService;
	
	@PostMapping("/place-order")
	public ResponseEntity<OrderResponse> placeOrder(@RequestBody Order order) {
		
		OrderResponse response = new OrderResponse();
		ArrayList<Order> orderList = new ArrayList<>();
		orderList.add(orderService.placeOrder(order));
		response.setListOfOrder(orderList);
		response.setStatus(HttpStatus.CREATED.toString());
		return new ResponseEntity<OrderResponse>( response, HttpStatus.CREATED);
	}
	
	@GetMapping("/all-order")
	public ResponseEntity<List<Order>> getAllOrderDetails() {
		return new ResponseEntity<List<Order>> (orderService.getAllOrderDetails(), HttpStatus.OK);		
	}
}
