package com.ncteam.ncordermanager.bean;

import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRN_ORDER_MANAGER")
public class Order {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderID;
	private int quantity;
	private double qPrice;
	private double qDiscount;
	private double finalPrice;
	
	

	public Order(long orderID, int quantity, double qPrice, double qDiscount, double finalPrice) {
		super();
		this.orderID = orderID;
		this.quantity = quantity;
		this.qPrice = qPrice;
		this.qDiscount = qDiscount;
		this.finalPrice = finalPrice;
		
	}
	
	public Order() {
		
	}

	public long getOrderID() {
		return orderID;
	}

	public void setOrderID(long orderID) {
		this.orderID = orderID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getqPrice() {
		return qPrice;
	}

	public void setqPrice(double qPrice) {
		this.qPrice = qPrice;
	}

	public double getqDiscount() {
		return qDiscount;
	}

	public void setqDiscount(double qDiscount) {
		this.qDiscount = qDiscount;
	}

	public double getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(double finalPrice) {
		this.finalPrice = finalPrice;
	}

	

	
}
